import React, { Fragment } from "react";

import Field from "../components/Field";
import Controller from "./Controller";

import withCounterLogic from "../withCounterLogic";

const Counter = ({
  type,
  color,
  count,
  handleChange,
  handleDecrement,
  handleIncrement,
  handleReset
}) => (
  <Fragment>
    {type === "input" ? (
      <Field value={count} onChange={e => handleChange(e)} />
    ) : (
      <p>Count: {count}</p>
    )}
    <Controller
      color={count < 3 ? color : "#A4031F"}
      onDecrement={handleDecrement}
      onIncrement={handleIncrement}
      onReset={handleReset}
    />
  </Fragment>
);

export default withCounterLogic(Counter);
