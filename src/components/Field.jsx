import React from "react";
import { Input, Label, Wrapper } from "../styles";

const Field = ({ value, onChange }) => (
  <Wrapper>
    <Label htmlFor="count">Count:</Label>
    <Input
      type="number"
      name="count"
      value={value}
      onChange={e => onChange(e)}
    />
  </Wrapper>
);

export default Field;
