import React from "react";
import { Button, Wrapper } from "../styles";

const useButton = (name, color) => {
  const type = "button";

  return {
    name,
    type,
    color
  };
};

const Controller = ({ color, onDecrement, onIncrement, onReset }) => {
  const decrement = useButton("decrement", color);
  const increment = useButton("increment", color);
  const reset = useButton("reset", color);

  return (
    <Wrapper>
      <Button onClick={onDecrement} {...decrement}>
        -
      </Button>
      <Button onClick={onIncrement} {...increment}>
        +
      </Button>
      <Button onClick={onReset} {...reset}>
        reset
      </Button>
    </Wrapper>
  );
};

export default Controller;
