import React from "react";
import ReactDOM from "react-dom";

import { color, GlobalStyle, Background } from "./styles";

import Counter from "./components/Counter";

const App = () => (
  <Background>
    <GlobalStyle />
    <div>
      <Counter type="input" color={color.lapis} />
    </div>
  </Background>
);

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
