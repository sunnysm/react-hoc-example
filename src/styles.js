import styled, { createGlobalStyle } from "styled-components";

const color = {
  black: "#070902",
  blue: "#86BBD8",
  lapis: "#336699",
  snow: "#FBF5F3",
  white: "#FDFBFA"
};

const Background = styled.div`
  width: 100%;
  height: 100vh;
  background-color: ${color.snow};
  padding: 100px;

  > div {
    width: 300px;
    height: 30px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin: 30px 0;

    p {
      font-size: 13px;
      text-transform: uppercase;
      color: ${color.black};
    }
  }
`;

const Wrapper = styled.div`
  display: flex;
`;

const Button = styled.button`
  font-family: "Lato", sans-serif;
  font-size: 11px;
  color: ${color.snow};
  text-transform: uppercase;

  background-color: ${({ color }) => `${color}`};
  border: 1px solid ${({ color }) => `${color}`};
  border-radius: 2px;

  margin: 5px;

  &:hover {
    background-color: ${color.blue};
    border-color: ${color.blue};
  }
`;

const Label = styled.label`
  font-size: 13px;
  text-transform: uppercase;
  color: ${color.black};
`;

const Input = styled.input`
  width: 80px;

  font-size: 13px;
  color: ${color.black};

  background-color: ${color.white};
  border: 1px solid ${color.lapis};

  margin: 0 5px;
  padding: 0 5px;
`;

const GlobalStyle = createGlobalStyle`  
  * {
    box-sizing: border-box;
    -webkit-tap-highlight-color: transparent;
    outline-color: ${color.lapis};
  }
  
  body {
    font-family: "Lato", sans-serif;
    color: ${color.black};
    line-height: 20px;
    margin: 0;
  }
  
  ::selection {
    color: ${color.snow};
    background-color: ${color.lapis};
  }  
`;

export { color, GlobalStyle, Background, Wrapper, Button, Label, Input };
