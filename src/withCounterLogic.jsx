import React, { Component } from "react";

const withCounterLogic = EnhancedComponent => {
  return class extends Component {
    state = { count: 0 };

    handleIncrement = () =>
      this.setState(prevState => ({ count: prevState.count + 1 }));

    handleDecrement = () =>
      this.setState(prevState => ({ count: prevState.count - 1 }));

    handleReset = () => this.setState({ count: 0 });

    handleChange = e => {
      const { value } = e.target;
      this.setState({ count: parseInt(value, 0) });
    };

    render() {
      const { count } = this.state;
      const { type } = this.props;

      return (
        <EnhancedComponent
          type={type}
          count={count}
          handleIncrement={this.handleIncrement}
          handleDecrement={this.handleDecrement}
          handleReset={this.handleReset}
          handleChange={e => this.handleChange(e)}
          {...this.props}
        />
      );
    }
  };
};

export default withCounterLogic;
